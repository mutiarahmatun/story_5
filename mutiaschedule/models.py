from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class Message(models.Model):
	hari = models.CharField(max_length=27, null = True)
	nama_kegiatan = models.CharField(max_length=27, null = True)
	tempat = models.CharField(max_length=27, null = True)
	kategori = models.CharField(max_length=27, null = True)
	waktu = models.DateTimeField(default=timezone.now)