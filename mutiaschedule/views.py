from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponseRedirect
from .forms import PostForm
from .models import Message



# Create your views here.

def home(request):
	return render(request, 'home.html')

def schedule(request):
	events = Message.objects.all().values()
	if(request.method == 'POST'):
		form = PostForm(request.POST or None)
		if(form.is_valid()):
			form.save()
			return HttpResponseRedirect(reverse('schedule'))
	else:
		form = Message()
	response = {'form' : form, 'events' : events }
	return render(request, 'forms.html', response)


def delete(request):
	Message.objects.all().delete() 
	return HttpResponseRedirect(reverse('schedule'))

def showForm(request):
	response = {}
	response['form'] = PostForm
	response['events'] = Message.objects.all()
	return render(request, 'forms.html', response)