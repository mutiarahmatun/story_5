from django.contrib import admin
from django.urls import path, re_path, include
from mutiaschedule import views

# app_name = 'mutiaschedule'

urlpatterns = [
	path('schedule/', views.showForm, name='schedule'),
	path('delete/', views.delete, name='delete'),
	re_path(r'^$', views.home, name='home'),
	path('addActivity/', views.schedule, name='addActivity')
]