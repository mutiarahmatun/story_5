from django import forms

from .models import Message

class PostForm(forms.ModelForm):

    class Meta:
        model = Message
        fields = ('hari', 'nama_kegiatan', 'tempat', 'kategori', 'waktu',)